import UIKit

struct Amount: Codable {
    
    let value: Double
    let currencyISO: String
    
    enum CodingKeys : String, CodingKey {
        case value
        case currencyISO = "currency_iso"
    }
    
    func getCurrencySymbol() -> String {
        let result = Locale.availableIdentifiers
            .map { Locale(identifier: $0) }
            .first { $0.currencyCode == currencyISO }
        if let currencySymbol = result?.currencySymbol {
            return currencySymbol
        } else {
            return currencyISO
        }
    }
    
}
