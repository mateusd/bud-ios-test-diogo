struct Product: Codable {
    
    let id: Double
    let title: String
    let icon: String
    
}
