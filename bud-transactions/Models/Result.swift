import UIKit

enum Result<Value> {
    case success(Value)
    case failure(Error)
    
    func resolve() throws -> Value {
        switch self {
        case .success(let value):
            return value
        case .failure(let error):
            throw error
        }
    }
}

extension Result where Value == Data {
    
    func decoded<T: Decodable>(dateFormat: String?) throws -> T {
        let decoder = JSONDecoder()
        let data = try resolve()
        if let dateFormat = dateFormat {
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormat
            decoder.dateDecodingStrategy = .formatted(formatter)
        }
        return try decoder.decode(T.self, from: data)
    }
    
}
