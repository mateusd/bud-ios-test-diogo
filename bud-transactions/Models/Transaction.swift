import UIKit

struct Transaction: Codable {
    
    let id: String
    let date: Date
    let description: String
    let currency: String
    let product: Product
    let amount: Amount
    
}
