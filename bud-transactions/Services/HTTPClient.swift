import UIKit

class HTTPClient {
    
    static func get(url: URL, handler: @escaping (Result<Data>) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                handler(.failure(error))
            } else {
                handler(.success(data!))
            }
            }.resume()
    }

}
