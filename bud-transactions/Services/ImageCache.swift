import UIKit
import Foundation

class ImageCache {
    
    private static let imageCache = NSCache<NSString, UIImage>()
    
    static func getImage(url: URL) -> UIImage? {
        return imageCache.object(forKey: url.absoluteString as NSString)
    }
    
    static func saveImage(image: UIImage, url: URL) {
        imageCache.setObject(image, forKey: url.absoluteString as NSString)
    }
    
}
