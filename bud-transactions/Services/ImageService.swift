import UIKit
import Foundation

class ImageService {
    
    enum ImageServiceError: Error {
        case invalidData
    }
    
    static func getImage(url: URL, handler: @escaping (Result<UIImage>) -> Void) {
        if let cachedImage = ImageCache.getImage(url: url) {
            handler(.success(cachedImage))
        } else {
            // Download and save in Cache
            self.downloadImage(url: url) { result in
                switch result {
                case .success(let image):
                    ImageCache.saveImage(image: image, url: url)
                    handler(.success(image))
                case .failure(let error):
                    handler(.failure(error))
                }
            }
        }
    }
    
    private static func downloadImage(url: URL, handler: @escaping (Result<UIImage>) -> Void) {
        HTTPClient.get(url: url) { result in
            switch result {
            case .success(let data):
                if let image = UIImage(data: data) {
                    handler(.success(image))
                } else {
                    handler(.failure(ImageServiceError.invalidData))
                }
            case .failure(let error):
                handler(.failure(error))
            }
        }
    }
    
}
