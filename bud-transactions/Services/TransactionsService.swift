import UIKit

class TransactionsService {
    
    enum TransactionsError: Error {
        case wrongUrl
    }
    
    static func getTransactions(handler: @escaping (Result<[Transaction]>) -> Void) {
        guard let url = URL(string: Constants.API.url) else {
            handler(.failure(TransactionsError.wrongUrl))
            return
        }
        
        HTTPClient.get(url: url) { result in
            switch result {
            case .success(_):
                do {
                    let transactionsResponse = try result.decoded(dateFormat: "yyyy-MM-dd") as TransactionsResponse
                    handler(.success(transactionsResponse.data))
                } catch let parsingError {
                    handler(.failure(parsingError))
                }
            case .failure(let error):
                handler(.failure(error))
            }
        }
    }
    
}
