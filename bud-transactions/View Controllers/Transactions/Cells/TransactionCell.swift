import UIKit

class TransactionCell: UITableViewCell {
    
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var iconImageView: UIImageView!
    
    static let identifier = String(describing: TransactionCell())
    private let dateFormatterPrint = DateFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        dateFormatterPrint.dateFormat = "dd MMM"
    }
    
    func setTransaction(transaction: Transaction) {
        descriptionLabel.text = transaction.description
        dateLabel.text = dateFormatterPrint.string(from: transaction.date)
        priceLabel.text = transaction.amount.getCurrencySymbol() + String(format:"%.2f", transaction.amount.value)
        if let url = URL(string: transaction.product.icon) {
            ImageService.getImage(url: url) { result in
                DispatchQueue.main.async{ [weak self] in
                    if case let .success(image) = result {
                        self?.iconImageView.image = image
                    }
                }
            }
        }
    }
    
}
