import UIKit

class TransactionsViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    private var transactions: [Transaction] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "TransactionCell", bundle: nil), forCellReuseIdentifier: TransactionCell.identifier)
        presentTransactions()
    }
    
    @IBAction private func pressedRefresh(_ sender: Any) {
        presentTransactions()
    }
    
    private func presentTransactions() {
        activityIndicator.startAnimating()
        TransactionsService.getTransactions { result in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                switch result {
                case .success(let transactions):
                    self.transactions = transactions
                case .failure(let error):
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    self.transactions = []
                }
                self.tableView.reloadData()
            }
        }
    }
    
}

extension TransactionsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TransactionCell.identifier, for: indexPath as IndexPath) as! TransactionCell
        cell.setTransaction(transaction: transactions[indexPath.row])
        return cell
    }
    
}
