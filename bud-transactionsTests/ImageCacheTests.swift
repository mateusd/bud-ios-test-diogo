import XCTest
@testable import bud_transactions

class ImageCacheTests: XCTestCase {
    
    func testParsing() {
        guard let filePath = Bundle(for: type(of: self)).path(forResource: "lloyds_icon", ofType: "jpg") else {
            XCTFail("Couldn't find image")
            return
        }
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe)
            let image = UIImage(data: data)
            let url = URL(string: filePath)
            
            ImageCache.saveImage(image: image!, url: url!)            
            let retrievedImage = ImageCache.getImage(url: url!)
            XCTAssertNotNil(retrievedImage)
        } catch {
            XCTFail("Failed to load image")
        }
    }
    
}
