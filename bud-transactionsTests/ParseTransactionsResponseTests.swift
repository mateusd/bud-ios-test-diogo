import XCTest
@testable import bud_transactions

class ParseTransactionsResponseTests: XCTestCase {
    
    func testParsing() {
        if let filePath = Bundle(for: type(of: self)).path(forResource: "response", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe)
                let result = Result<Data>.success(data)
                do {
                    let response = try result.decoded(dateFormat: "yyyy-MM-dd") as TransactionsResponse
                    XCTAssertEqual(response.data.count, 10)
                    XCTAssertEqual(response.data.first?.description, "Forbidden planet")
                    XCTAssertEqual(response.data.first?.product.title, "Lloyds Bank")
                } catch {
                    XCTFail("Failed to decode")
                }
            } catch {
                XCTFail("Failed to load file")
            }
        } else {
            XCTFail("Couldn't find response")
        }
    }
    
}
